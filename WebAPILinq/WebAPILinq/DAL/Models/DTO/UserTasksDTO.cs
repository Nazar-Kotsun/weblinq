using System;
using System.Collections.Generic;

namespace WebAPILinq.DAL.Models
{
    public class UserTasksDTO
    {
        public int UserId { get; set; }
        public string UserFirstName { get; set; }
        public string UserLastName { get; set; }
        public string Email { get; set; }
        public DateTime Birthday { get; set; }
        public DateTime UserRegisteredAt { get; set; }
        public int? TeamId { get; set; }

        public IEnumerable<TaskModel> ListTasks { get; set; }
    }
}