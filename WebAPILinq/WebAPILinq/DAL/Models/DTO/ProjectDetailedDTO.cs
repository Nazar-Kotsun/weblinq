

namespace WebAPILinq.DAL.Models
{
    public class ProjectDetailedDTO
    {
        public ProjectModel Project { get; set; }
        public TaskModel TheLongestTask{ get; set; }
        public TaskModel TheShortedTask { get; set; }
        public int? CountOfUsers { get; set; }
    }
}