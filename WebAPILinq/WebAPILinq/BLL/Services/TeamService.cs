using System;
using System.Collections.Generic;
using System.Linq;
using WebAPILinq.DAL.Models;
using WebAPILinq.UnitOfWork;

namespace WebAPILinq.BLL.Services
{
    public class TeamService: ITeamService
    {
        private readonly IUnitOfWork _unitOfWork;

        public TeamService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public IEnumerable<TeamModel> GetAllTeams()
        {
            return _unitOfWork.TeamRepository.GetAll();
        }
        public IEnumerable<TeamUsersDTO> GetTeamsWithUsers()
        {
            var result = GetAllTeams()
                .GroupJoin(
                    _unitOfWork.UserRepository.GetAll().OrderByDescending(u => u.RegisteredAt),
                    team => team.Id,
                    user => user.TeamId,
                    (team, user) => new TeamUsersDTO()
                    {
                        TeamId = team.Id,
                        TeamName = team.Name,
                        ListUsers = user
                    }
                ).Where(team => team.ListUsers
                    .All(user => DateTime.Now.Year - user.Birthday.Year > 10));
            
            return result;
        }

        public void AddTeam(TeamModel teamModel)
        {
            if (teamModel != null)
            {
                if (_unitOfWork.TeamRepository.GetById(teamModel.Id) != null)
                {
                    throw new InvalidOperationException("This team already exists");
                }
                
                _unitOfWork.TeamRepository.Create(teamModel);
            }
            else
            {
                throw new NullReferenceException();
            }
        }

        public void DeleteTeam(int teamId)
        {
            if (_unitOfWork.TeamRepository.GetById(teamId) == null)
            {
                throw new InvalidOperationException("This team doesn't exist");
            }
            
            List<ProjectModel> projects = _unitOfWork.ProjectRepository.GetAll().ToList();
            List<UserModel> users = _unitOfWork.UserRepository.GetAll().ToList();
            
            for (int i = 0; i < projects.Count; i++)
            {
                if (projects[i].TeamId == teamId)
                {
                    _unitOfWork.ProjectRepository.Delete(projects[i].Id);
                }
            }

            for (int i = 0; i < users.Count; i++)
            {
                if (users[i].TeamId == teamId)
                {
                    users[i].TeamId = null;
                }
            }
            
            _unitOfWork.TeamRepository.Delete(teamId);
        }

        public void UpdateTeam(TeamModel teamModel)
        {
            if (teamModel != null)
            {
                if (_unitOfWork.TeamRepository.GetById(teamModel.Id) == null)
                { 
                    throw new InvalidOperationException("This team doesn't exist");
                }
                
                _unitOfWork.TeamRepository.Update(teamModel);
            }

            else
            {
                throw new NullReferenceException();
            }
        }
    }
}