using System;
using System.Collections.Generic;
using System.Linq;
using WebAPILinq.DAL.Models;
using WebAPILinq.Helpers;

namespace WebAPILinq.UnitOfWork.Repositories
{
    public class TaskRepository : IBaseRepository<TaskModel>
    {
        private List<TaskModel> _tasks;
        public TaskRepository()
        {
            _tasks = TaskModelFactory.CreateTasks().ToList();
        }

        public void Create(TaskModel entity) => _tasks.Add(entity);
        
        public TaskModel GetById(int id) => _tasks.Find((task) => task.Id == id);

        public IEnumerable<TaskModel> GetAll() => _tasks;

        public void Update(TaskModel entity)
        {
            Delete(entity.Id);
            Create(entity);
        }

        public void Delete(int id) => _tasks.Remove(_tasks.Find((task) => task.Id == id));

    }
}