using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;


namespace WebAPILinq.DAL.Models
{
    public class ProjectModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime Deadline { get; set; }
        public int AuthorId { get; set; }
        public int TeamId { get; set; }
        public List<TaskModel> Tasks { get; set; }

        public override string ToString()
        {
            return $"Id: {Id}\n" +
                   $"Name: {Name}\n" +
                   $"Description: {Description}\n" +
                   $"CreatedAt: {CreatedAt}\n" +
                   $"Deadline: {Deadline}\n" +
                   $"AuthorId: {AuthorId}\n" +
                   $"TeamId: {TeamId}";
        }
    }
}