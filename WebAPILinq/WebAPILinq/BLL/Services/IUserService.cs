using System.Collections.Generic;
using WebAPILinq.DAL.Models;

namespace WebAPILinq.BLL.Services
{
    public interface IUserService
    {
        IEnumerable<UserModel> GetAllUsers();

        IEnumerable<UserTasksDTO> GetUsersWithSortedTasks();
        UserDetailedDTO GetDetailedInformByUserId(int userId);
        public bool IsExist(int userId);
        void AddUser(UserModel user);

        void DeleteUser(int userId);

        void UpdateUser(UserModel userModel);
    }
}