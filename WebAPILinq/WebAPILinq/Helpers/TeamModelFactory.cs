using System;
using System.Collections.Generic;
using WebAPILinq.DAL.Models;

namespace WebAPILinq.Helpers
{
    public static class TeamModelFactory
    {
        public static IEnumerable<TeamModel> CreateTeams()
        {
            return new List<TeamModel>
            {
                new TeamModel
                {
                    Id = 1,
                    Name = "debitis",
                    CreatedAt = Convert.ToDateTime("2020-06-30T20:20:46.3968861+00:00")
                },
                new TeamModel
                {
                    Id = 2,
                    Name = "cupiditate",
                    CreatedAt = Convert.ToDateTime("2020-06-30T18:46:57.0010953+00:00")
                },
                new TeamModel
                {
                    Id = 3,
                    Name = "asperiores",
                    CreatedAt = Convert.ToDateTime("2020-07-01T03:02:23.1538591+00:00")
                },
                new TeamModel
                {
                    Id = 4,
                    Name = "ea",
                    CreatedAt = Convert.ToDateTime("2020-07-01T04:38:51.3579944+00:00")
                },
                new TeamModel
                {
                    Id = 5,
                    Name = "amet",
                    CreatedAt = Convert.ToDateTime("2020-06-30T15:19:41.6189004+00:00")
                }
            };
        } 
    }
}

