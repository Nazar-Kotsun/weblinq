using System.Collections.Generic;
using System.Linq;
using WebAPILinq.DAL.Models;
using WebAPILinq.Helpers;

namespace WebAPILinq.UnitOfWork.Repositories
{
    public class TeamRepository: IBaseRepository<TeamModel>
    {
        private List<TeamModel> _teams;

        public TeamRepository()
        {
            _teams = TeamModelFactory.CreateTeams().ToList();
        }

        public void Create(TeamModel entity) => _teams.Add(entity);

        public TeamModel GetById(int id) => _teams.Find((team) => team.Id == id);
        public IEnumerable<TeamModel> GetAll() => _teams;

        public void Update(TeamModel entity)
        {
           Delete(entity.Id);
           Create(entity);
        }

        public void Delete(int id) => _teams.Remove
            (_teams.Find((team) => team.Id == id)
        );
    }
}