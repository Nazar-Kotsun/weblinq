using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPILinq.DAL.Models;
using WebAPILinq.UnitOfWork;

namespace WebAPILinq.BLL.Services
{
    public class TaskService : ITaskService
    {
        private readonly IUnitOfWork _unitOfWork;

        public TaskService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IEnumerable<TaskModel> GetAllTasks()
        {
            var result = _unitOfWork.TaskRepository.GetAll();
            return result;
        }

        public IEnumerable<TaskModel> GetTasksByUserId(int userId)
        {
            var result = GetAllTasks()
                .Where(task => task.PerformerId == userId && task.Name.Length < 45);
            return result;
        }

        public IEnumerable<TaskDTO> GetTasksFinishedInCurrentYear(int userId)
        {
            var result = GetAllTasks()
                .Where(task => task.FinishedAt.Year == DateTime.Now.Year && task.PerformerId == userId)
                .Select(task => new TaskDTO{Id = task.Id, Name = task.Name});
            
            return result;
        }

        public void AddTask(TaskModel taskModel)
        {
            if (taskModel != null)
            {
                if (_unitOfWork.TaskRepository.GetById(taskModel.Id) != null)
                { 
                    throw new InvalidOperationException("This task already exist");
                }
                
                if (_unitOfWork.UserRepository.GetById(taskModel.PerformerId) == null)
                {
                    throw new InvalidOperationException("This user doesn't exist");
                }

                if (_unitOfWork.ProjectRepository.GetById(taskModel.ProjectId) == null)
                {
                    throw new InvalidOperationException("This project doesn't exist");
                }

                _unitOfWork.TaskRepository.Create(taskModel);
            }

            else
            {
                throw new NullReferenceException();
            }
        }

        public void DeleteTask(int taskId)
        {
            if (_unitOfWork.TaskRepository.GetById(taskId) == null)
            {
                throw new InvalidOperationException("This task doesn't exist");
            }
            
            _unitOfWork.TaskRepository.Delete(taskId);
        }

        public void UpdateTask(TaskModel taskModel)
        {
            if (taskModel != null)
            {
                
                if (_unitOfWork.TaskRepository.GetById(taskModel.Id) == null)
                { 
                    throw new InvalidOperationException("This task doesn't exist");
                }
                
                if (_unitOfWork.UserRepository.GetById(taskModel.PerformerId) == null)
                {
                    throw new InvalidOperationException("This performer doesn't exist");
                }

                if (_unitOfWork.ProjectRepository.GetById(taskModel.ProjectId) == null)
                {
                    throw new InvalidOperationException("This project doesn't exist");
                }
                
                _unitOfWork.TaskRepository.Update(taskModel);
            }

            else
            {
                throw new NullReferenceException();
            }
        }
    }
}