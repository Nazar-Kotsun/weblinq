using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace LInqClient.Models
{
    public class Team
    {
        [Required] 
        [JsonPropertyName("id")] 
        public int Id { get; set; }

        [Required] 
        [JsonPropertyName("name")] 
        public string Name { get; set; }

        [Required] 
        [JsonPropertyName("createdAt")] 
        public DateTime CreatedAt { get; set; }

        public List<User> Users { get; set; }

        public override string ToString()
        {
            return $"Id: {Id}\n" +
                   $"Name: {Name}\n" +
                   $"CreatedAt: {CreatedAt}";
        }
    }
}