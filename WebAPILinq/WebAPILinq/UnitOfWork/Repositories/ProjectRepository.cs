using System;
using System.Collections.Generic;
using System.Linq;
using WebAPILinq.DAL.Models;
using WebAPILinq.Helpers;

namespace WebAPILinq.UnitOfWork.Repositories
{
    public class ProjectRepository : IBaseRepository<ProjectModel>
    {
        private List<ProjectModel> _projects;

        public ProjectRepository()
        {
            _projects = ProjectModelFactory.CreateProjects().ToList();
        }

        public void Create(ProjectModel entity) => _projects.Add(entity);

        public ProjectModel GetById(int id) => _projects.Find((project) => project.Id == id);

        public IEnumerable<ProjectModel> GetAll() => _projects;

        public void Update(ProjectModel entity)
        {
            Delete(entity.Id);
            Create(entity);
        }

        public void Delete(int id) => _projects.Remove(
            _projects.Find((project) => project.Id == id)
            );
    }
}