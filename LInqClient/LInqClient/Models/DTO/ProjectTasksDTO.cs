using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace LInqClient.Models
{
    public class ProjectTasksDTO
    {
        [Required] 
        [JsonPropertyName("projectId")] 
        public int ProjectId { get; set; }

        [Required] 
        [JsonPropertyName("projectName")] 
        public string ProjectName { get; set; }
        
        [Required] 
        [JsonPropertyName("authorId")] 
        public int AuthorId { get; set; }
        
        [Required] 
        [JsonPropertyName("teamId")] 
        public int TeamId { get; set; }
        
        [Required]
        [JsonPropertyName("countOfTasks")]
        public int CountOfTasks { get; set; }

        public override string ToString()
        {
            return $"Id: {ProjectId}\n" +
                   $"Name: {ProjectName}\n" +
                   $"AuthorId: {AuthorId}\n" +
                   $"TeamId: {TeamId}\n" +
                   $"CountOfTasks: {CountOfTasks}" ;
        }
    }
}