using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using LInqClient.DTO;
using LInqClient.HttpLinqClient;
using LInqClient.Models;
using LInqClient.Models.DTO;
using Microsoft.VisualBasic.FileIO;


namespace LInqClient.Menu
{
    public class MainMenu
    {
        private LinqClientRequest _lInqClient;

        private bool exit = false;
        private int choice;
        
        public MainMenu()
        {
            Initialization();
        }
        private void Initialization()
        {
           _lInqClient = new LinqClientRequest();
        }
        private void DrawSeparatorLine(string text)
        {
            Console.WriteLine("------------" + text + "------------");
        }
        private void StopProgramToContinue()
        {
            Console.Write("Please enter any key to continue...");
            Console.ReadKey();
            Console.Clear();
        }
        public async Task StartMenu()
        {
            while (!exit)
            {
               await ShowMenu();
            }
        }
        private async Task ShowMenu()
        {   
            Console.WriteLine("1 - Get dictionary(key - project; value - count of tasks) -> (task_1)");
            Console.WriteLine("2 - Get the list of tasks where name is less than 45 characters -> (task_2)");
            Console.WriteLine("3 - Get the list (id,name) of the tasks that ended in 2020 -> (task_3)");
            Console.WriteLine("4 - Get the list (id, name of team, list users) of the teams whose members\n" +
                              "are older than 10 year -> (task_4)");
            Console.WriteLine("5 - Get the list of users alphabetically with sorted tasks by length \n "
                              +"  of name -> (task_5)");
            Console.WriteLine("6 - Get more detailed information about user -> (task_6)");
            Console.WriteLine("7 - Get more detailed information about project -> (task_7)");
            Console.WriteLine("8 - Add new user");
            Console.WriteLine("9 - Delete user");
            Console.WriteLine("10 - Update user");
            Console.WriteLine("11 - Add new project");
            Console.WriteLine("12 - Delete project");
            Console.WriteLine("13 - Update project");
            Console.WriteLine("14 - Add new task");
            Console.WriteLine("15 - Delete task");
            Console.WriteLine("16 - Update task");
            Console.WriteLine("17 - Add new team");
            Console.WriteLine("18 - Delete team");
            Console.WriteLine("19 - Update team");
            Console.WriteLine("0 - Exit");
            await DoChoice();
        }

        private void CheckCorrectEnteringInteger(out int value)
        {
            while (!int.TryParse(Console.ReadLine(), out value))
            {
                Console.WriteLine("Incorrect entering!");
            }
        }
        private async Task DoChoice()
        {
            Console.Write("Your choice: ");
            CheckCorrectEnteringInteger(out choice);
            
            switch (choice)
            {
                case 1:
                {
                    await GetProjectsWithTasks();
                    StopProgramToContinue();
                    break;
                }
                case 2:
                {
                    await GetTasksByUserId();
                    StopProgramToContinue();
                    break;
                }
                case 3:
                {
                    await GetTasksFinishedInCurrentYear();
                    StopProgramToContinue();
                    break;
                }
                case 4:
                {
                    await GetTeamsWithUsers();
                    StopProgramToContinue();
                    break;
                }
                case 5:
                {
                    await GetUsersWithSortedTasks();
                    StopProgramToContinue();
                    break;
                }
                case 6:
                {
                    await GetUserDetailedDTO();
                    StopProgramToContinue();
                    break;
                }
                case 7:
                {
                    await GetProjectsDetailedInfo();
                    StopProgramToContinue();
                    break;
                }
                case 8:
                {
                    await AddUser();
                    StopProgramToContinue();
                    break;
                }
                case 9:
                {
                    await DeleteUser();
                    StopProgramToContinue();
                    break;
                }
                case 10:
                {
                    await UpdateUser();
                    StopProgramToContinue();
                    break;
                }
                case 11:
                {
                    await AddProject();
                    StopProgramToContinue();
                    break;
                }
                case 12:
                {
                    await DeleteProject();
                    StopProgramToContinue();
                    break;
                }
                case 13:
                {
                    await UpdateProject();
                    StopProgramToContinue();
                    break;
                }
                case 14:
                {
                    await AddTask();
                    StopProgramToContinue();
                    break;
                }
                case 15:
                {
                    await DeleteTask();
                    StopProgramToContinue();
                    break;
                }
                case 16:
                {
                    await UpdateTask();
                    StopProgramToContinue();
                    break;
                }
                case 17:
                {
                    await AddTeam();
                    StopProgramToContinue();
                    break;
                }
                case 18:
                {
                    await DeleteTeam();
                    StopProgramToContinue();
                    break;
                }
                case 19:
                {
                    await UpdateTeam();
                    StopProgramToContinue();
                    break;
                }
                case 0:
                {
                    Exit();
                    break;
                }
            }
        }
        private async Task GetProjectsWithTasks()
        {
            try
            {
                int userId;
                Console.Write("Please, enter user id: ");
                CheckCorrectEnteringInteger(out userId);

                IEnumerable<ProjectTasksDTO> projectTasksDtosDtos =
                    await _lInqClient.GetProjectsWithTasksByUserId(userId);

                int i = 1;
                foreach (var project in projectTasksDtosDtos)
                {
                    DrawSeparatorLine("Project - " + i);
                    Console.WriteLine(project);
                    i++;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private async Task GetTasksByUserId()
        {
            try
            {
                int userId;
                Console.Write("Please, enter user id: ");
                CheckCorrectEnteringInteger(out userId);

                IEnumerable<TaskModel> tasks = await _lInqClient.GetTasksByUSerId(userId);

                int i = 1;
                foreach (var task in tasks)
                {
                    DrawSeparatorLine("Task - " + i);
                    Console.WriteLine(task);
                    i++;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private async Task GetTasksFinishedInCurrentYear()
        {
            try
            {
                int userId;
                Console.Write("Please, enter user id: ");
                CheckCorrectEnteringInteger(out userId);

                IEnumerable<TaskDTO> tasksDtos = await _lInqClient.GetTasksFinishedInCurrentYear(userId);
                
                int i = 1;
                foreach (var task in tasksDtos)
                {
                    DrawSeparatorLine("Task - " + i);
                    Console.WriteLine(task);
                    i++;
                }
                DrawSeparatorLine("--------");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private async Task GetTeamsWithUsers()
        {
            try
            {
                IEnumerable<TeamUsersDTO> teamDtos = await _lInqClient.GetTeamsWithUsers();

                int i= 1;
                int j = 1;
                
                foreach (var team in teamDtos)
                {
                    DrawSeparatorLine("Team - " + i);
                    Console.WriteLine(team);
                    
                    j = 1;
                    foreach (var user in team.ListUsers)
                    {
                        DrawSeparatorLine("User - " + j);
                        Console.WriteLine(user);
                        j++;
                    }
                    i++;
                }
                DrawSeparatorLine("-------");
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        
        private async Task GetUsersWithSortedTasks()
        {
            try
            {
                IEnumerable<UserTasksDTO> teamDtos = await _lInqClient.GetUsersWithSortedTasks();

                int i= 1;
                int j = 1;
                
                foreach (var team in teamDtos)
                {
                    DrawSeparatorLine("User - " + i);
                    Console.WriteLine(team);
                    
                    j = 1;
                    foreach (var task in team.ListTasks)
                    {
                        DrawSeparatorLine("Task - " + j);
                        Console.WriteLine(task);
                        j++;
                    }
                    i++;
                }
                DrawSeparatorLine("-------");
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public async Task GetUserDetailedDTO()
        {
            try
            {
                int userId;
                Console.Write("Please, enter user id: ");
                CheckCorrectEnteringInteger(out userId);
                
                UserDetailedDTO userInfo = await _lInqClient.GetUsersDetailedInfo(userId);
                
                DrawSeparatorLine("User");
                Console.WriteLine(userInfo.User);
                DrawSeparatorLine("LastProject");
                Console.WriteLine(userInfo.LastProject);
                DrawSeparatorLine("CountOfTasks");
                Console.WriteLine(userInfo.CountOfTasks); 
                DrawSeparatorLine("CountOfStartedOrCanceledTasks");
                Console.WriteLine(userInfo.CountOfStartedOrCanceledTasks);
                DrawSeparatorLine("TheLongestTask");
                Console.WriteLine(userInfo.TheLongestTask);
               
                DrawSeparatorLine("---------");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public async Task GetProjectsDetailedInfo()
        {
            try
            {
                IEnumerable<ProjectDetailedDTO> projectDetailedDtos =  await _lInqClient.GetProjectsDetailedInfo();
                foreach (var project in projectDetailedDtos)
                {
                    DrawSeparatorLine("Project");
                    Console.WriteLine(project.Project);
                    DrawSeparatorLine("The longest task");
                    Console.WriteLine(project.TheLongestTask);
                    DrawSeparatorLine("The shorted task");
                    Console.WriteLine(project.TheShortedTask);
                    DrawSeparatorLine("Count of users in team");
                    Console.WriteLine(project.CountOfUsers);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public async Task AddUser()
        {
            try
            {
                User user = new User();
                Console.WriteLine("Please enter new user!");
                Console.Write("Id: ");
                user.Id = int.Parse(Console.ReadLine());
                Console.Write("FirstName: ");
                user.FirstName = Console.ReadLine();
                Console.Write("LastName: ");
                user.LastName = Console.ReadLine();
                Console.Write("Email: ");
                user.Email = Console.ReadLine();
                Console.Write("Birthday: ");
                user.Birthday = Convert.ToDateTime(Console.ReadLine());
                Console.Write("RegisteredAt: ");
                user.RegisteredAt = Convert.ToDateTime(Console.ReadLine());
                Console.Write("TeamId: ");
                user.TeamId = int.Parse(Console.ReadLine());
                string strMessage = await _lInqClient.AddUser(user);
                Console.WriteLine(strMessage);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public async Task DeleteUser()
        {
            int userId;
            Console.Write("Please, enter user id: ");
            CheckCorrectEnteringInteger(out userId);

            string strMessage = await _lInqClient.DeleteUser(userId);
            Console.WriteLine(strMessage);
        }
        
        public async Task UpdateUser()
        {
            try
            {
                User user = new User();
                Console.WriteLine("Please enter new user!");
                Console.Write("Id: ");
                user.Id = int.Parse(Console.ReadLine());
                Console.Write("FirstName: ");
                user.FirstName = Console.ReadLine();
                Console.Write("LastName: ");
                user.LastName = Console.ReadLine();
                Console.Write("Email: ");
                user.Email = Console.ReadLine();
                Console.Write("Birthday: ");
                user.Birthday = Convert.ToDateTime(Console.ReadLine());
                Console.Write("RegisteredAt: ");
                user.RegisteredAt = Convert.ToDateTime(Console.ReadLine());
                Console.Write("TeamId: ");
                user.TeamId = int.Parse(Console.ReadLine());
                string strMessage = await _lInqClient.UpdateUser(user);
                Console.WriteLine(strMessage);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        
        public async Task AddProject()
        {
            try
            {
                Project project = new Project();
                Console.WriteLine("Please enter new project!");
                Console.Write("Id: ");
                project.Id = int.Parse(Console.ReadLine());
                Console.Write("Name: ");
                project.Name = Console.ReadLine();
                Console.Write("Description: ");
                project.Description = Console.ReadLine();
                Console.Write("CreatedAt: ");
                project.CreatedAt = Convert.ToDateTime(Console.ReadLine());
                Console.Write("Deadline: ");
                project.Deadline = Convert.ToDateTime(Console.ReadLine());
                Console.Write("AuthorId: ");
                project.AuthorId = int.Parse(Console.ReadLine());
                Console.Write("TeamId: ");
                project.TeamId = int.Parse(Console.ReadLine());

                string strMessage = await _lInqClient.AddProject(project);
                Console.WriteLine(strMessage);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        
        public async Task DeleteProject()
        {
            int projectId;
            Console.Write("Please, enter project id: ");
            CheckCorrectEnteringInteger(out projectId);

            string strMessage = await _lInqClient.DeleteProject(projectId);
            Console.WriteLine(strMessage);
        }
        
        public async Task UpdateProject()
        {
            try
            {
                Project project = new Project();
                Console.WriteLine("Please enter new project!");
                Console.Write("Id: ");
                project.Id = int.Parse(Console.ReadLine());
                Console.Write("Name: ");
                project.Name = Console.ReadLine();
                Console.Write("Description: ");
                project.Description = Console.ReadLine();
                Console.Write("CreatedAt: ");
                project.CreatedAt = Convert.ToDateTime(Console.ReadLine());
                Console.Write("Deadline: ");
                project.Deadline = Convert.ToDateTime(Console.ReadLine());
                Console.Write("AuthorId: ");
                project.AuthorId = int.Parse(Console.ReadLine());
                Console.Write("TeamId: ");
                project.TeamId = int.Parse(Console.ReadLine());

                string strMessage = await _lInqClient.UpdateProject(project);
                Console.WriteLine(strMessage);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        
        public async Task AddTask()
        {
            try
            {
                TaskModel taskModel = new TaskModel();
                Console.WriteLine("Please enter new task!");
                Console.Write("Id: ");
                taskModel.Id = int.Parse(Console.ReadLine());
                Console.Write("Name: ");
                taskModel.Name = Console.ReadLine();
                Console.Write("Description: ");
                taskModel.Description = Console.ReadLine();
                
                Console.Write("State: ");
                int taskState;
                CheckCorrectEnteringInteger(out taskState);
                switch (taskState)
                {
                    case 0:
                    {
                        taskModel.State = TaskState.Created;
                        break;
                    }
                    case 1:
                    {
                        taskModel.State = TaskState.Started;
                        break;
                    }
                    case 2:
                    {
                        taskModel.State = TaskState.Finished;
                        break;
                    }
                    case 3:
                    {
                        taskModel.State = TaskState.Canceled;
                        break;
                    }
                    default:
                    {
                        taskModel.State = TaskState.Created;
                        break;
                    }
                }
                Console.Write("CreatedAt: ");
                taskModel.CreatedAt = Convert.ToDateTime(Console.ReadLine());
                Console.Write("FinishedAt: ");
                taskModel.FinishedAt = Convert.ToDateTime(Console.ReadLine());
                Console.Write("ProjectId: ");
                taskModel.ProjectId = int.Parse(Console.ReadLine());
                Console.Write("PerformerId: ");
                taskModel.PerformerId = int.Parse(Console.ReadLine());

                string strMessage = await _lInqClient.AddTask(taskModel);
                Console.WriteLine(strMessage);
                
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        
        public async Task DeleteTask()
        {
            int taskId;
            Console.Write("Please, enter task id: ");
            CheckCorrectEnteringInteger(out taskId);

            string strMessage = await _lInqClient.DeleteTask(taskId);
            Console.WriteLine(strMessage);
        }
        
        public async Task UpdateTask()
        {
            try
            {
                TaskModel taskModel = new TaskModel();
                Console.WriteLine("Please enter new task!");
                Console.Write("Id: ");
                taskModel.Id = int.Parse(Console.ReadLine());
                Console.Write("Name: ");
                taskModel.Name = Console.ReadLine();
                Console.Write("Description: ");
                taskModel.Description = Console.ReadLine();
                
                Console.Write("State: ");
                int taskState;
                CheckCorrectEnteringInteger(out taskState);
                switch (taskState)
                {
                    case 0:
                    {
                        taskModel.State = TaskState.Created;
                        break;
                    }
                    case 1:
                    {
                        taskModel.State = TaskState.Started;
                        break;
                    }
                    case 2:
                    {
                        taskModel.State = TaskState.Finished;
                        break;
                    }
                    case 3:
                    {
                        taskModel.State = TaskState.Canceled;
                        break;
                    }
                    default:
                    {
                        taskModel.State = TaskState.Created;
                        break;
                    }
                }
                Console.Write("CreatedAt: ");
                taskModel.CreatedAt = Convert.ToDateTime(Console.ReadLine());
                Console.Write("FinishedAt: ");
                taskModel.FinishedAt = Convert.ToDateTime(Console.ReadLine());
                Console.Write("ProjectId: ");
                taskModel.ProjectId = int.Parse(Console.ReadLine());
                Console.Write("PerformerId: ");
                taskModel.PerformerId = int.Parse(Console.ReadLine());

                string strMessage = await _lInqClient.UpdateTask(taskModel);
                Console.WriteLine(strMessage);
                
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        
        public async Task AddTeam()
        {
            try
            {
                Team team = new Team();
                Console.WriteLine("Please enter new team!");
                Console.Write("Id: ");
                team.Id = int.Parse(Console.ReadLine());
                Console.Write("Name: ");
                team.Name = Console.ReadLine();
                Console.Write("CreatedAt: ");
                team.CreatedAt = Convert.ToDateTime(Console.ReadLine());
                
                string strMessage = await _lInqClient.AddTeam(team);
                Console.WriteLine(strMessage);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        
        public async Task DeleteTeam()
        {
            int teamId;
            Console.Write("Please, enter team id: ");
            CheckCorrectEnteringInteger(out teamId);

            string strMessage = await _lInqClient.DeleteTeam(teamId);
            Console.WriteLine(strMessage);
        }
        
        public async Task UpdateTeam()
        {
            try
            {
                Team team = new Team();
                Console.WriteLine("Please enter new team!");
                Console.Write("Id: ");
                team.Id = int.Parse(Console.ReadLine());
                Console.Write("Name: ");
                team.Name = Console.ReadLine();
                Console.Write("CreatedAt: ");
                team.CreatedAt = Convert.ToDateTime(Console.ReadLine());
                
                string strMessage = await _lInqClient.UpdateTeam(team);
                Console.WriteLine(strMessage);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        
        private void Exit()
        {
            exit = true;
        }
    }
}
    