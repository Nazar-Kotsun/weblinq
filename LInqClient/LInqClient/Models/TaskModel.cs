using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace LInqClient.Models
{
    public class TaskModel
    {
        [Required] 
        [JsonPropertyName("id")] 
        public int Id { get; set; }

        [Required] 
        [JsonPropertyName("name")] 
        public string Name { get; set; }
        
        [Required] 
        [JsonPropertyName("description")] 
        public string Description { get; set; }
        
        [Required] 
        [JsonPropertyName("state")] 
        public TaskState State { get; set; }
        
        [Required] 
        [JsonPropertyName("createdAt")] 
        public DateTime CreatedAt { get; set; }
        
        [Required] 
        [JsonPropertyName("finishedAt")] 
        public DateTime FinishedAt { get; set; }
        
        [Required] 
        [JsonPropertyName("projectId")] 
        public int ProjectId { get; set; }
        
        [Required] 
        [JsonPropertyName("performerId")] 
        public int PerformerId { get; set; }
        //
        // public User Performer { get; set; }

        public override string ToString()
        {
            return $"Id: {Id}\n" +
                   $"Name: {Name}\n" +
                   $"Description: {Description}\n" +
                   $"State: {State}\n" +
                   $"CreatedAt: {CreatedAt}\n" +
                   $"FinishedAt: {FinishedAt}\n" +
                   $"ProjectId: {ProjectId}\n" +
                   $"PerformerId: {PerformerId}";
        }
        
    }
}