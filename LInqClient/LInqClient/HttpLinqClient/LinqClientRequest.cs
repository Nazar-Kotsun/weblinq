using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using LInqClient.DTO;
using LInqClient.Models;
using LInqClient.Models.DTO;


namespace LInqClient.HttpLinqClient
{
    public class LinqClientRequest
    {
        private HttpClient client;
        private HttpResponseMessage responseMessage;

        public LinqClientRequest()
        {
            HttpClientHandler httpClientHandler = new HttpClientHandler();
            httpClientHandler.ServerCertificateCustomValidationCallback =
                (httpRequestMessage, cert, cetChain, policyErrors) => true;
            client = new HttpClient(httpClientHandler);
            client.BaseAddress = new Uri("https://localhost:5001/api/");
        }

        public async Task<IEnumerable<Project>> GetProjects()
        {
            responseMessage = await client.GetAsync("projects");
            return JsonSerializer.Deserialize<IEnumerable<Project>>(await responseMessage.Content.ReadAsStringAsync());
        }

        public async Task<IEnumerable<ProjectTasksDTO>> GetProjectsWithTasksByUserId(int userId)
        {
            responseMessage = await client.GetAsync("projects/projectsWithTasks/" + userId);
            if (responseMessage.IsSuccessStatusCode)
            {
                return JsonSerializer.Deserialize<IEnumerable<ProjectTasksDTO>>(
                    await responseMessage.Content.ReadAsStringAsync());
            }

            throw new Exception(responseMessage.StatusCode.ToString() + "\n" +
                                responseMessage.Content.ReadAsStringAsync().Result);
        }

        public async Task<IEnumerable<TaskModel>> GetTasksByUSerId(int userId)
        {
            responseMessage = await client.GetAsync("tasks/byUserId/" + userId);
            if (responseMessage.IsSuccessStatusCode)
            {
                return JsonSerializer.Deserialize<IEnumerable<TaskModel>>(await responseMessage.Content
                    .ReadAsStringAsync());
            }

            throw new Exception(responseMessage.StatusCode.ToString());
        }

        public async Task<IEnumerable<TaskDTO>> GetTasksFinishedInCurrentYear(int userId)
        {
            responseMessage = await client.GetAsync("tasks/finishedInCurrentYear/" + userId);
            if (responseMessage.IsSuccessStatusCode)
            {
                return JsonSerializer.Deserialize<IEnumerable<TaskDTO>>(await responseMessage.Content
                    .ReadAsStringAsync());
            }

            throw new Exception(responseMessage.StatusCode.ToString());
        }

        public async Task<IEnumerable<TeamUsersDTO>> GetTeamsWithUsers()
        {
            responseMessage = await client.GetAsync("teams/withUsers");
            if (responseMessage.IsSuccessStatusCode)
            {
                return JsonSerializer.Deserialize<IEnumerable<TeamUsersDTO>>(await responseMessage.Content
                    .ReadAsStringAsync());
            }

            throw new Exception(responseMessage.StatusCode.ToString());
        }

        public async Task<IEnumerable<UserTasksDTO>> GetUsersWithSortedTasks()
        {
            responseMessage = await client.GetAsync("users/withSortedTasks");
            if (responseMessage.IsSuccessStatusCode)
            {
                return JsonSerializer.Deserialize<IEnumerable<UserTasksDTO>>(await responseMessage.Content
                    .ReadAsStringAsync());
            }

            throw new Exception(responseMessage.StatusCode.ToString());
        }

        public async Task<UserDetailedDTO> GetUsersDetailedInfo(int userId)
        {
            responseMessage = await client.GetAsync("users/detailedInfo/" + userId);
            if (responseMessage.IsSuccessStatusCode)
            {
                return JsonSerializer.Deserialize<UserDetailedDTO>(await responseMessage.Content
                    .ReadAsStringAsync());
            }

            throw new Exception(responseMessage.StatusCode.ToString() + '\n' 
                                                                      + responseMessage.Content.ReadAsStringAsync().Result);
        }
        
        public async Task<IEnumerable<ProjectDetailedDTO>> GetProjectsDetailedInfo()
        {
            responseMessage = await client.GetAsync("projects/detailedInfo");
            if (responseMessage.IsSuccessStatusCode)
            {
                return JsonSerializer.Deserialize<IEnumerable<ProjectDetailedDTO>>(await responseMessage.Content
                    .ReadAsStringAsync());
            }

            throw new Exception(responseMessage.StatusCode.ToString());
        }

        public async Task<string> AddUser(User user)
        {
            if (user != null)
            {
                string strSerializeVehicle = JsonSerializer.Serialize<User>(user);

                HttpContent httpContent = new StringContent(strSerializeVehicle, Encoding.UTF8, "application/json");
                responseMessage = await client.PostAsync("users", httpContent);
    
                if (responseMessage.IsSuccessStatusCode)
                {
                    Console.WriteLine("Success!");
                }
                
                return await responseMessage.Content.ReadAsStringAsync();
                
            }
            else
            {
                throw new NullReferenceException();
            }
        }
        
        public async Task<string> DeleteUser(int userId)
        {  
             responseMessage = await client.DeleteAsync("/api/users/" + userId);

             return await responseMessage.Content.ReadAsStringAsync();
        }
        
        public async Task<string> UpdateUser(User user)
        {  
            string strSerialize = JsonSerializer.Serialize<User>(user);
            HttpContent httpContent = new StringContent(strSerialize, Encoding.UTF8, "application/json");
            
            responseMessage = await client.PutAsync("users", httpContent);
            
            return await responseMessage.Content.ReadAsStringAsync();
        }
        
        public async Task<string> AddProject(Project project)
        {
            if (project != null)
            {
                string strSerializeVehicle = JsonSerializer.Serialize<Project>(project);

                HttpContent httpContent = new StringContent(strSerializeVehicle, Encoding.UTF8, "application/json");
                responseMessage = await client.PostAsync("projects", httpContent);
    
                if (responseMessage.IsSuccessStatusCode)
                {
                    Console.WriteLine("Success!");
                }
                
                return await responseMessage.Content.ReadAsStringAsync();
                
            }
            else
            {
                throw new NullReferenceException();
            }
        }
        
        public async Task<string> DeleteProject(int projectId)
        {  
            responseMessage = await client.DeleteAsync("projects/" + projectId);

            return await responseMessage.Content.ReadAsStringAsync();
        }
        
        public async Task<string> UpdateProject(Project project)
        {  
            string strSerialize = JsonSerializer.Serialize<Project>(project);
            HttpContent httpContent = new StringContent(strSerialize, Encoding.UTF8, "application/json");
            
            responseMessage = await client.PutAsync("projects", httpContent);
            
            return await responseMessage.Content.ReadAsStringAsync();
        }
        
        public async Task<string> AddTask(TaskModel taskModel)
        {
            if (taskModel != null)
            {
                string strSerializeVehicle = JsonSerializer.Serialize<TaskModel>(taskModel);

                HttpContent httpContent = new StringContent(strSerializeVehicle, Encoding.UTF8, "application/json");
                responseMessage = await client.PostAsync("tasks", httpContent);
    
                if (responseMessage.IsSuccessStatusCode)
                {
                    Console.WriteLine("Success!");
                }
                
                return await responseMessage.Content.ReadAsStringAsync();
            }
            else
            {
                throw new NullReferenceException();
            }
        }
        public async Task<string> DeleteTask(int taskId)
        {  
            responseMessage = await client.DeleteAsync("tasks/" + taskId);

            return await responseMessage.Content.ReadAsStringAsync();
        }
        
        public async Task<string> UpdateTask(TaskModel taskModel)
        {  
            string strSerialize = JsonSerializer.Serialize<TaskModel>(taskModel);
            HttpContent httpContent = new StringContent(strSerialize, Encoding.UTF8, "application/json");
            
            responseMessage = await client.PutAsync("tasks", httpContent);
            
            return await responseMessage.Content.ReadAsStringAsync();
        }
        
        public async Task<string> AddTeam(Team team)
        {
            if (team != null)
            {
                string strSerializeVehicle = JsonSerializer.Serialize<Team>(team);

                HttpContent httpContent = new StringContent(strSerializeVehicle, Encoding.UTF8, "application/json");
                responseMessage = await client.PostAsync("teams", httpContent);
    
                if (responseMessage.IsSuccessStatusCode)
                {
                    Console.WriteLine("Success!");
                }
                
                return await responseMessage.Content.ReadAsStringAsync();
            }
            else
            {
                throw new NullReferenceException();
            }
        }
        
        public async Task<string> DeleteTeam(int teamId)
        {  
            responseMessage = await client.DeleteAsync("teams/" + teamId);

            return await responseMessage.Content.ReadAsStringAsync();
        }
        
        public async Task<string> UpdateTeam(Team team)
        {  
            string strSerialize = JsonSerializer.Serialize<Team>(team);
            HttpContent httpContent = new StringContent(strSerialize, Encoding.UTF8, "application/json");
            
            responseMessage = await client.PutAsync("teams", httpContent);
            
            return await responseMessage.Content.ReadAsStringAsync();
        }
        
    }
    
}