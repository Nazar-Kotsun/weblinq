using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace WebAPILinq.UnitOfWork.Repositories
{
    public interface IBaseRepository<T>
    {
        void Create(T entity);
        T GetById(int id);
        IEnumerable<T> GetAll();
        void Update(T entity);
        void Delete(int id);
    }
}