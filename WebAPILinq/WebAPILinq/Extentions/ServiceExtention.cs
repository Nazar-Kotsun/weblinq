using Microsoft.Extensions.DependencyInjection;
using WebAPILinq.BLL.Services;
using WebAPILinq.DAL.Models;
using WebAPILinq.UnitOfWork;

namespace WebAPILinq.Extentions
{
    public static class ServiceExtention
    {
        public static void AddProjectServices(this IServiceCollection services)
        {
            services.AddSingleton<ITaskService, TaskService>();
            services.AddSingleton<IProjectService, ProjectService>();
            services.AddSingleton<IUserService, UserService>();
            services.AddSingleton<ITeamService, TeamService>();
            services.AddSingleton<IUnitOfWork, UnitOfWork.UnitOfWork>();
        } 
        
    }
}