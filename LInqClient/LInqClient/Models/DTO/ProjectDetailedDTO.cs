using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace LInqClient.Models.DTO
{
    public class ProjectDetailedDTO
    {
        [Required]
        [JsonPropertyName("project")]
        public Project Project { get; set; }
        [Required]
        [JsonPropertyName("theLongestTask")]
        public TaskModel TheLongestTask{ get; set; }
        [Required]
        [JsonPropertyName("theShortedTask")]
        public TaskModel TheShortedTask { get; set; }
        [Required]
        [JsonPropertyName("countOfUsers")]
        public int? CountOfUsers { get; set; }
    }
}