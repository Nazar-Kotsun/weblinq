using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using LInqClient.Models;

namespace LInqClient.DTO
{
    public class TeamUsersDTO
    {
        [Required]
        [JsonPropertyName("teamId")]
        public int TeamId { get; set; }
        [Required]
        [JsonPropertyName("name")]
        public string TeamName { get; set; }
        [Required]
        [JsonPropertyName("listUsers")]
        public IEnumerable<User> ListUsers { get; set; }

        public override string ToString()
        {
            return $"TeamId: {TeamId}\n" +
                   $"TeamName: {TeamName}\n" +
                   $"Count list of users: {ListUsers.ToList().Count}";
        }
    }
}