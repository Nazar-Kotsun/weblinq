using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace LInqClient.Models.DTO
{
    public class UserDetailedDTO
    {
        [Required]
        [JsonPropertyName("user")]
        public User User { get; set; }
        [Required]
        [JsonPropertyName("lastProject")]
        public Project LastProject { get; set; }
        [Required]
        [JsonPropertyName("countOfTasks")]
        public int CountOfTasks { get; set; }
        [Required]
        [JsonPropertyName("countOfStartedOrCanceledTasks")]
        public int CountOfStartedOrCanceledTasks { get; set; }
        [Required]
        [JsonPropertyName("theLongestTask")]
        public TaskModel TheLongestTask { get; set; }
        
    }
}