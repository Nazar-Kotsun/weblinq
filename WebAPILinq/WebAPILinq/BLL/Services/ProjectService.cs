using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using WebAPILinq.DAL.Models;
using WebAPILinq.UnitOfWork;

namespace WebAPILinq.BLL.Services
{
    public class ProjectService : IProjectService
    {
        private readonly IUnitOfWork _unitOfWork;

        public ProjectService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public IEnumerable<ProjectModel> GetAllProjects()
        {
            return _unitOfWork.ProjectRepository.GetAll();
        }

        public IEnumerable<ProjectTasksDTO> GetProjectsWithCountOfTasksByUserId(int userId)
        {
            var result = GetAllProjects()
                .GroupJoin(_unitOfWork.TaskRepository.GetAll(),
                    project => project.Id,
                    task => task.ProjectId,
                    (project, task) => new ProjectTasksDTO()
                    {
                        ProjectId = project.Id,
                        ProjectName = project.Name,
                        AuthorId = project.AuthorId,
                        TeamId = project.TeamId,
                        CountOfTasks = task.ToList().Count
                    }).Where(project => project.AuthorId == userId);
            
            return result;
        }
        
        public IEnumerable<ProjectDetailedDTO> GetProjectsDetailed()
        {
            var result = _unitOfWork.ProjectRepository.GetAll()
                .GroupJoin(_unitOfWork.TaskRepository.GetAll(),
                    project => project.Id,
                    task => task.ProjectId,
                    (project, tasks) => new
                    {
                        Project = project,
                        CountOfTAsks = tasks.ToList().Count,
                        MaxTask =  tasks
                            .OrderByDescending(task => task.Description.Length)
                            .FirstOrDefault(),
                        MinTask = tasks
                            .OrderBy(task => task.Name.Length)
                            .FirstOrDefault(),
                    }
                ).GroupJoin(_unitOfWork.UserRepository.GetAll(),
                    project => project.Project.TeamId,
                    user => user.TeamId,
                    (project, users) => new ProjectDetailedDTO()
                    {
                        Project = project.Project,
                        TheLongestTask = project.MaxTask,
                        TheShortedTask = project.MinTask,
                        CountOfUsers = users
                            .Where(user => user.TeamId == project.Project.TeamId 
                                                           && project.Project.Description.Length > 20 
                                                           && project.CountOfTAsks < 3).ToList().Count
                    }
                    );
            return result;
        }

        public void AddProject(ProjectModel projectModel)
        {
            if (projectModel != null)
            {
                if (_unitOfWork.ProjectRepository.GetById(projectModel.Id) != null)
                {
                    throw new InvalidOperationException("This project already exists");
                }
                if (projectModel.TeamId != null)
                {
                    object obj = projectModel.TeamId;
                    if (_unitOfWork.TeamRepository.GetById((int) obj) == null)
                    {
                        throw new InvalidOperationException("This team doesn't exist");
                    }
                }
                if (_unitOfWork.UserRepository.GetById(projectModel.AuthorId) == null)
                {
                    throw new InvalidOperationException("This user doesn't exist");
                }
                
                _unitOfWork.ProjectRepository.Create(projectModel);
            }
            else
            {
                throw new NullReferenceException();
            }
        }

        public void DeleteProject(int projectId)
        {
            if (_unitOfWork.ProjectRepository.GetById(projectId) == null)
            {
                throw new InvalidOperationException("This project doesn't exist");
            }
            
            List<TaskModel> tasks = _unitOfWork.TaskRepository.GetAll().ToList();

            for (int i = 0; i < tasks.Count; i++)
            {
                if (tasks[i].ProjectId == projectId)
                {
                    _unitOfWork.TaskRepository.Delete(tasks[i].Id);
                }
            }
            
            _unitOfWork.ProjectRepository.Delete(projectId);
        }

        public void UpdateProject(ProjectModel projectModel)
        {
            if (projectModel != null)
            {
                if (_unitOfWork.UserRepository.GetById(projectModel.AuthorId) == null)
                {
                    throw new InvalidOperationException("This user doesn't exist");
                }

                if (_unitOfWork.ProjectRepository.GetById(projectModel.Id) == null)
                {
                    throw new InvalidOperationException("This project doesn't exist");
                }
                
                if (_unitOfWork.TeamRepository.GetById(projectModel.TeamId) == null)
                { 
                    throw new InvalidOperationException("This team doesn't exist");
                }
                
                _unitOfWork.ProjectRepository.Update(projectModel);
            }

            else
            {
                throw new NullReferenceException();
            }
        }
    }
}