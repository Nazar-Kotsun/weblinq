using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace LInqClient.Models
{
    public class Project
    {
        [Required] 
        [JsonPropertyName("id")] 
        public int Id { get; set; }

        [Required] 
        [JsonPropertyName("name")] 
        public string Name { get; set; }
        
        [Required] 
        [JsonPropertyName("description")] 
        public string Description { get; set; }
        
        [Required] 
        [JsonPropertyName("createdAt")] 
        public DateTime CreatedAt { get; set; }
        
        [Required] 
        [JsonPropertyName("deadline")] 
        public DateTime Deadline { get; set; }
        
        [Required] 
        [JsonPropertyName("authorId")] 
        public int AuthorId { get; set; }
        
        [Required] 
        [JsonPropertyName("teamId")] 
        public int TeamId { get; set; }

        public List<TaskModel> Tasks { get; set; }
        public User Author { get; set; }
        public Team? Team { get; set; }

        public override string ToString()
        {
            return $"Id: {Id}\n" +
                   $"Name: {Name}\n" +
                   $"Description: {Description}\n" +
                   $"CreatedAt: {CreatedAt}\n" +
                   $"Deadline: {Deadline}\n" +
                   $"AuthorId: {AuthorId}\n" +
                   $"TeamId: {TeamId}";
        }
    }
}