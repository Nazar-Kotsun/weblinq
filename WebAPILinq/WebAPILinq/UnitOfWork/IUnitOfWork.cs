using WebAPILinq.DAL.Models;
using WebAPILinq.UnitOfWork.Repositories;

namespace WebAPILinq.UnitOfWork
{
    public interface IUnitOfWork
    {
        IBaseRepository<TaskModel> TaskRepository { get; }
        IBaseRepository<ProjectModel> ProjectRepository { get; }
        IBaseRepository<UserModel> UserRepository { get; }
        IBaseRepository<TeamModel> TeamRepository { get; }
    }
}