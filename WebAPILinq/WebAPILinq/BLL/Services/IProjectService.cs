using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using WebAPILinq.DAL.Models;

namespace WebAPILinq.BLL.Services
{
    public interface IProjectService
    {
        IEnumerable<ProjectModel> GetAllProjects();
        IEnumerable<ProjectTasksDTO> GetProjectsWithCountOfTasksByUserId(int userId);
        IEnumerable<ProjectDetailedDTO> GetProjectsDetailed();
        void AddProject(ProjectModel project);

        void DeleteProject(int projectId);

        void UpdateProject(ProjectModel projectModel);
    }
}