using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace LInqClient.Models
{
    public class User
    {
        [Required] 
        [JsonPropertyName("id")] 
        public int Id { get; set; }
        
        [Required] 
        [JsonPropertyName("firstName")] 
        public string FirstName { get; set; }
        
        [Required] 
        [JsonPropertyName("lastName")] 
        public string LastName { get; set; }
        
        [Required] 
        [JsonPropertyName("email")] 
        public string Email { get; set; }
        
        [Required] 
        [JsonPropertyName("birthday")] 
        public DateTime Birthday { get; set; }
        
        [Required] 
        [JsonPropertyName("registeredAt")] 
        public DateTime RegisteredAt { get; set; }
        
        [Required] 
        [JsonPropertyName("teamId")] 
        public int? TeamId { get; set; }

        // public Team UserTeams { get; set; }
        
        public override string ToString()
        {
            return $"Id: {Id}\n" +
                   $"FirstName: {FirstName}\n" +
                   $"LastName: {LastName}\n" +
                   $"Email: {Email}\n" +
                   $"Birthday: {Birthday}\n" +
                   $"RegisteredAt: {RegisteredAt}\n" +
                   $"TeamId: {TeamId}";
        }
        
    }
}