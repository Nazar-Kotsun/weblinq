using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using WebAPILinq.BLL.Services;
using WebAPILinq.DAL.Models;

namespace WebAPILinq.Controllers
{
    [ApiController]
    [Route("api/")]
    public class ProjectController : Controller
    {
        
        private readonly IProjectService _projectService;

        public ProjectController(IProjectService projectService)
        {
            _projectService = projectService;
        }

        [HttpGet]
        [Route("projects")]
        public IActionResult GetAll()
        {
            var result = _projectService.GetAllProjects();
            
            if (result.Any())
            {
                return Ok(result);
            }
            else
            {
                return NotFound();
            }
        }

        [HttpGet]
        [Route("projects/projectsWithTasks/{userId}")]

        public IActionResult GetProjectsWithTasks(int userId)
        {
            var result = _projectService.GetProjectsWithCountOfTasksByUserId(userId);
            
            if (result.Any())
            {
                return Ok(result);
            }
            else
            {
                return NotFound("This user doesn't exist or doesn't have any projects");
            }
        }
        
        [HttpGet]
        [Route("projects/detailedInfo")]
        public IActionResult GetProjectsDetaildeInfo()
        {
            var result = _projectService.GetProjectsDetailed();
            
            if (result.Any())
            {
                return Ok(result);
            }
            else
            {
                return NotFound();
            }
        }
        
        [HttpPost]
        [Route("projects")]
        public IActionResult AddProject([FromBody] ProjectModel projectModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Body request is bad");
            }
            
            string strMistakes;
            
            try
            {
                _projectService.AddProject(projectModel);
                return Created("", "Project was created!");
            }
            catch (InvalidCastException e)
            {
                strMistakes = e.Message;
            }
            catch (NullReferenceException e)
            {
                strMistakes = e.Message;
            }
            catch (Exception e)
            {
                strMistakes = e.Message;
            }

            return BadRequest(strMistakes);
        }
        
        [HttpDelete]
        [Route("projects/{projectId}")]
        public IActionResult DeleteProject(int projectId)
        {
            string strMistakes;
            try
            {
                _projectService.DeleteProject(projectId);
                return Ok("Project was deleted");
            }
            catch (InvalidOperationException ex)
            {
                strMistakes = ex.Message;
            }
            catch (NullReferenceException ex)
            {
                strMistakes = ex.Message;
            }
            catch (Exception ex)
            {
                strMistakes = ex.Message;
            }

            return BadRequest(strMistakes);
        }
        
        [HttpPut]
        [Route("projects")]
        public IActionResult UpdateProject(ProjectModel projectModel)
        {
            string strMistakes;
            
            if (!ModelState.IsValid)
            {
                return BadRequest("Body request is bad");
            }
            
            try
            {
                _projectService.UpdateProject(projectModel);
                return Ok("Project was updated");
            }
            catch (InvalidCastException e)
            {
                strMistakes = e.Message;
            }
            catch (NullReferenceException e)
            {
                strMistakes = e.Message;
            }
            catch (Exception e)
            {
                strMistakes = e.Message;
            }

            return BadRequest(strMistakes);
        }
        
    }
}