using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Threading.Tasks;
using System.Xml.Schema;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Mvc;
using WebAPILinq.DAL.Models;
using WebAPILinq.UnitOfWork;

namespace WebAPILinq.BLL.Services
{
    public class UserService : IUserService
    {

        private readonly IUnitOfWork _unitOfWork;

        public UserService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IEnumerable<UserModel> GetAllUsers()
        {
            return _unitOfWork.UserRepository.GetAll();
        }

        public bool IsExist(int userId)
        {
            return _unitOfWork.UserRepository.GetAll().ToList().Find(user => user.Id == userId) != null;
        }
        
        public IEnumerable<UserTasksDTO> GetUsersWithSortedTasks()
        {
            var result = GetAllUsers()
                .GroupJoin(
                    _unitOfWork.TaskRepository.GetAll()
                        .OrderByDescending(task => task.Name.Length),
                    user => user.Id,
                    task => task.PerformerId,
                    (user, tasks) => new UserTasksDTO()
                    {
                        UserId = user.Id,
                        UserFirstName = user.FirstName,
                        UserLastName = user.LastName,
                        Email = user.Email,
                        Birthday = user.Birthday,
                        UserRegisteredAt = user.RegisteredAt,
                        TeamId = user.TeamId,
                        ListTasks = tasks
                    }
                ).OrderBy(user => user.UserFirstName);
            return result;
        }
        
        public UserDetailedDTO GetDetailedInformByUserId(int userId)
        {
            var projects = _unitOfWork.ProjectRepository.GetAll().GroupJoin(_unitOfWork.TaskRepository.GetAll(),
                proj => proj.Id,
                task => task.ProjectId,
                (proj, tasks) => new ProjectModel()
                {
                    Id = proj.Id,
                    Name = proj.Name,
                    Description = proj.Description,
                    AuthorId = proj.AuthorId,
                    CreatedAt = proj.CreatedAt,
                    Deadline = proj.Deadline,
                    Tasks = tasks.ToList()
                });
            
            UserDetailedDTO result = null;
            try
            {
                result = new UserDetailedDTO
                {
                    User = _unitOfWork.UserRepository.GetAll()
                        .Where(user => user.Id == userId).FirstOrDefault(),
                    LastProject = projects.Where(project => project.AuthorId == userId)
                        .OrderByDescending(project => project.CreatedAt).FirstOrDefault(),
                    CountOfTasks = projects.Where(project => project.AuthorId == userId)
                        .OrderByDescending(project => project.CreatedAt).FirstOrDefault().Tasks.ToList().Count,
                    CountOfStartedOrCanceledTasks = _unitOfWork.TaskRepository.GetAll()
                        .Count(task => task.PerformerId == userId &&
                                       (task.State == TaskState.Started || task.State == TaskState.Canceled)),
                    TheLongestTask = _unitOfWork.TaskRepository.GetAll().Where(task => task.PerformerId == userId)
                        .OrderByDescending(task => task.FinishedAt - task.CreatedAt)
                        .FirstOrDefault()
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine("This user doesn't have any projects");
            }

            return result;
        }

        public void AddUser(UserModel user)
        {
            if (user != null)
            {
                if (_unitOfWork.UserRepository.GetById(user.Id) != null)
                {
                    throw new InvalidOperationException("This user already exists");
                }
                
                if (user.TeamId != null)
                {
                    object obj = user.TeamId;
                    if (_unitOfWork.TeamRepository.GetById((int) obj) == null)
                    {
                        throw new InvalidOperationException("This team doesn't exist");
                    }
                }
                 
                _unitOfWork.UserRepository.Create(user);
                
            }
            else
            {
                throw new NullReferenceException("User is null");
            }
        }

        public void DeleteUser(int userId)
        {
            if (_unitOfWork.UserRepository.GetById(userId) == null)
            {
                throw new InvalidOperationException("This user doesn't exist");
            }

            List<ProjectModel> projects = _unitOfWork.ProjectRepository.GetAll().ToList();
            List<TaskModel> tasks = _unitOfWork.TaskRepository.GetAll().ToList();
            
            int? projectDeleteId = null;
            
            for (int i = 0; i < projects.Count; i++)
            {
                projectDeleteId = null;
                if (projects[i].AuthorId == userId)
                {
                    _unitOfWork.ProjectRepository.Delete(projects[i].Id);
                    projectDeleteId = projects[i].Id;
                }
                
                for (int j = 0; j < tasks.Count; j++)
                {
                    if (tasks[j].ProjectId == projectDeleteId || tasks[j].PerformerId == userId)
                    {
                        _unitOfWork.TaskRepository.Delete(tasks[j].Id);
                    }
                }
            }
            
            _unitOfWork.UserRepository.Delete(userId);
        }

        public void UpdateUser(UserModel userModel)
        {
            if (userModel != null)
            {
                if (_unitOfWork.UserRepository.GetById(userModel.Id) == null)
                {
                    throw new InvalidOperationException("This user doesn't exist");
                }

                if (userModel.TeamId != null)
                {
                    object obj = userModel.TeamId;
                    if (_unitOfWork.TeamRepository.GetById((int) obj) == null)
                    {
                        throw new InvalidOperationException("This team doesn't exist");
                    }
                }
                
                _unitOfWork.UserRepository.Update(userModel);
            }

            else
            {
                throw new NullReferenceException();
            }

        }
    }
}