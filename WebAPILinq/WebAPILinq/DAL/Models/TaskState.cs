namespace WebAPILinq.DAL.Models
{
    public enum TaskState
    {
        Created = 0, Started, Finished, Canceled
    }
}