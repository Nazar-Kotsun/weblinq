using System;
using System.Collections.Generic;
using WebAPILinq.DAL.Models;

namespace WebAPILinq.Helpers
{
    public static class TaskModelFactory
    {
        public static IEnumerable<TaskModel> CreateTasks()
        {
            return new List<TaskModel>
            {
                new TaskModel
                {
                    Id = 1,
                    Name = "Quasi consectetur nesciunt doloribus.",
                    Description = "Sed voluptas quia dolores expedita eius laborum ut qui aspernatur.\n" +
                                  "Molestias sapiente pariatur fuga architecto sed.\n" +
                                  "Autem repellendus maxime magni qui exercitationem rerum.\n" +
                                  "Dolorem magnam aut commodi nemo aut quaerat.\n" +
                                  "Eos sit veniam qui molestiae facere voluptatem.\n" +
                                  "Facilis eum atque enim dolor facilis ea ipsum tempora.",
                    CreatedAt = Convert.ToDateTime("2020-06-30T22:22:09.9030937+00:00"),
                    FinishedAt = Convert.ToDateTime("2020-12-10T22:30:51.0724501+00:00"),
                    State = TaskState.Finished,
                    ProjectId = 1,
                    PerformerId = 1,
                },
                new TaskModel()
                {
                    Id = 2,
                    Name = "Praesentium ut consequatur cumque eveniet suscipit amet officia.",
                    Description = "Praesentium autem consequatur magnam et doloribus exercitationem.\n" +
                                  "Aut animi fuga cupiditate debitis atque nisi consequatur consequatur.\n" +
                                  "Cupiditate necessitatibus quo eos sequi earum et quis accusamus.",
                    CreatedAt = Convert.ToDateTime("2020-07-01T11:49:39.729857+00:00"),
                    FinishedAt = Convert.ToDateTime("2020-07-19T22:05:30.3390708+00:00"),
                    State = TaskState.Created,
                    ProjectId = 2,
                    PerformerId = 9
                },
                new TaskModel()
                {
                    Id = 3,
                    Name = "Error sit sunt.",
                    Description = "Unde dignissimos libero minima quas aliquam.\n" +
                                  "Consequuntur aliquid non.\n" +
                                  "Eligendi quia quidem nihil sit veritatis.",
                    CreatedAt = Convert.ToDateTime("2020-06-30T22:52:33.0645959+00:00"),
                    FinishedAt = Convert.ToDateTime("2020-07-25T07:38:48.6570938+00:00"),
                    State = TaskState.Canceled,
                    ProjectId = 3,
                    PerformerId = 14
                },
                new TaskModel()
                {
                    Id = 4,
                    Name = "Repellendus itaque expedita est ut.",
                    Description = "Nisi esse accusamus dolorem blanditiis porro est dolores.\n" +
                                  "Explicabo consequatur rem dignissimos odit praesentium.\n" +
                                  "Molestiae facilis et tenetur.\n" +
                                  "Voluptas quis sed et ab nulla omnis cupiditate.\n" +
                                  "Id sed et.",
                    CreatedAt = Convert.ToDateTime("2020-07-01T01:35:10.4670552+00:00"),
                    FinishedAt = Convert.ToDateTime("2020-10-19T05:59:38.2813374+00:00"),
                    State = TaskState.Finished,
                    ProjectId = 4,
                    PerformerId = 3
                },
                new TaskModel()
                {
                    Id = 5,
                    Name = "Voluptate neque vel molestiae dolor nulla voluptas voluptas optio.",
                    Description = "Rerum totam sit.\n" +
                                  "Velit saepe iusto et repellat et consequuntur sit.\n" +
                                  "Voluptate officiis pariatur ut ea.\n" +
                                  "Neque ut sed voluptatem occaecati.\n" +
                                  "Dolor velit quaerat molestiae assumenda veritatis voluptatem.",
                    CreatedAt = Convert.ToDateTime("2020-06-30T19:51:23.2733812+00:00"),
                    FinishedAt = Convert.ToDateTime("2021-06-20T02:07:03.7230486+00:00"),
                    State = TaskState.Started,
                    ProjectId = 5,
                    PerformerId = 16
                },
                new TaskModel()
                {
                    Id = 6,
                    Name = "In qui labore omnis voluptas voluptates aspernatur consectetur pariatur.",
                    Description = "Sit tenetur nihil laborum qui quia assumenda ratione.\n" +
                                  "Tempora esse deleniti quia debitis incidunt odio consequatur unde.\n" +
                                  "Et quos quam consectetur excepturi sint qui enim autem eaque.\n" +
                                  "Adipisci praesentium officia non quod vel rerum nihil.\n" +
                                  "Veniam qui incidunt dolorum.\n" +
                                  "Molestiae ea officia qui explicabo nulla repellat.",
                    CreatedAt = Convert.ToDateTime("2020-07-01T06:47:59.8929665+00:00"),
                    FinishedAt = Convert.ToDateTime("2020-10-26T16:26:22.2573206+00:00"),
                    State = TaskState.Canceled,
                    ProjectId = 6,
                    PerformerId = 2
                },
                new TaskModel()
                {
                    Id = 7,
                    Name = "Rerum eum qui ad repudiandae eum laborum ut saepe.",
                    Description = "Dolores esse quibusdam aut ut quidem nulla voluptatem.\n" +
                                  "Quidem vitae sequi aut qui cumque adipisci quo quam.\n" +
                                  "Alias quis voluptatibus.\n" +
                                  "Qui est aut.\nNihil quia occaecati occaecati totam laudantium.\n" +
                                  "Nobis cum quae saepe molestiae voluptas id reiciendis a consequuntur.",
                    CreatedAt = Convert.ToDateTime("2020-07-01T13:48:47.7325296+00:00"),
                    FinishedAt = Convert.ToDateTime("2021-06-16T14:09:33.1578012+00:00"),
                    State = TaskState.Started,
                    ProjectId = 7,
                    PerformerId = 13
                },
                new TaskModel()
                {
                    Id = 8,
                    Name = "Et ipsa voluptatem non eos et similique sunt.",
                    Description = "Nihil eos minima sed.\n" +
                                  "Est et assumenda voluptatem voluptatem illum doloribus.\n" +
                                  "Voluptatem enim voluptatem et ut.",
                    CreatedAt = Convert.ToDateTime("2020-07-01T12:54:51.5178046+00:00"),
                    FinishedAt = Convert.ToDateTime("2020-12-29T16:35:36.3138043+00:00"),
                    State = TaskState.Finished,
                    ProjectId = 8,
                    PerformerId = 16
                },
                new TaskModel()
                {
                    Id = 9,
                    Name = "Dolorem fugiat impedit nesciunt enim non.",
                    Description = "Ipsam quo soluta aut numquam aliquam sint.\n" +
                                  "Aliquam voluptas error fuga est et quae dolores.",
                    CreatedAt = Convert.ToDateTime("2020-06-30T15:10:02.9778316+00:00"),
                    FinishedAt = Convert.ToDateTime("2020-11-24T08:24:59.079391+00:00"),
                    State = TaskState.Created,
                    ProjectId = 9,
                    PerformerId = 3
                },
                new TaskModel()
                {
                    Id = 10,
                    Name = "Dolorem eos incidunt eum perspiciatis officia ratione est rerum.",
                    Description = "Quia quam eveniet quisquam rerum voluptatum laboriosam repudiandae.\n" +
                                  "Voluptate fugit esse eveniet ducimus sunt veniam a.\n" +
                                  "Sed qui et consequatur similique eum velit ipsa voluptates ut.",
                    CreatedAt = Convert.ToDateTime("2020-07-01T11:45:13.8346421+00:00"),
                    FinishedAt = Convert.ToDateTime("2020-10-13T06:22:02.7319262+00:00"),
                    State = TaskState.Finished,
                    ProjectId = 10,
                    PerformerId = 18
                },
                new TaskModel()
                {
                    Id = 11,
                    Name = "Quod autem atque similique",
                    Description = "Quod autem atque similique molestiae dicta quia.\n" +
                                  "Nulla nulla consequatur at sint enim et similique.\n" +
                                  "Fugit occaecati enim aut doloremque aliquid vero molestiae iste.\n" +
                                  "Quaerat delectus id",
                    CreatedAt = Convert.ToDateTime("2020-07-01T01:50:46.5659074+00:00"),
                    FinishedAt = Convert.ToDateTime("2020-12-23T07:07:45.7354976+00:00"),
                    State = TaskState.Started,
                    ProjectId = 8,
                    PerformerId = 17
                },
                new TaskModel()
                {
                    Id = 12,
                    Name = "Quidem dolores ut dolores atque sapiente laborum sint.",
                    Description = "Optio modi exercitationem quia in omnis alias.\n" +
                                  "Esse a voluptatem porro quo voluptatem fuga eos consequatur sit.\n" +
                                  "Repellendus labore excepturi eaque impedit minus rerum ut qui eum.\n" +
                                  "Odio enim qui corrupti.",
                    CreatedAt = Convert.ToDateTime("2020-07-01T12:38:02.3115268+00:00"),
                    FinishedAt = Convert.ToDateTime("2021-06-17T13:35:49.4661287+00:00"),
                    State = TaskState.Started,
                    ProjectId = 3,
                    PerformerId = 15 
                },
                new TaskModel()
                {
                    Id = 13,
                    Name = "Minima vel eum atque perspiciatis laborum officiis cum.",
                    Description = "Quas et quae optio ullam amet amet qui voluptatum.\n" +
                                  "Ut eos neque quia occaecati voluptas voluptatem modi consequatur doloribus.\n" +
                                  "Voluptatem occaecati et.\n" +
                                  "Fuga deserunt nam porro nam nobis deserunt laboriosam asperiores.\n" +
                                  "Autem voluptatem cumque amet totam ducimus unde officiis.\n" +
                                  "Earum aspernatur qui maxime at voluptatem placeat.",
                    CreatedAt = Convert.ToDateTime("2020-06-30T15:35:39.9513554+00:00"),
                    FinishedAt = Convert.ToDateTime("2021-02-22T02:41:05.7626966+00:00"),
                    State = TaskState.Finished,
                    ProjectId = 4,
                    PerformerId = 5
                },
                new TaskModel()
                {
                    Id = 14,
                    Name = "Placeat impedit ullam eveniet corporis quidem doloribus.",
                    Description = "Ipsum odio qui in dolorum aperiam ut.\n" +
                                  "Blanditiis ratione sapiente quos est quia pariatur.\n" +
                                  "Similique repudiandae hic enim non neque magnam fugiat est.\n" +
                                  "Excepturi accusamus non soluta inventore enim doloribus culpa veniam.\n" +
                                  "Impedit sunt magni cumque autem.\n" +
                                  "Quis suscipit culpa quia voluptatem.",
                    CreatedAt = Convert.ToDateTime("2020-06-30T19:04:36.6175319+00:00"),
                    FinishedAt = Convert.ToDateTime("2020-09-30T21:44:11.4086524+00:00"),
                    State = TaskState.Created,
                    ProjectId = 1,
                    PerformerId = 4
                },
                new TaskModel()
                {
                    Id = 15,
                    Name = "Qui facilis suscipit.",
                    Description = "Laborum omnis dicta.\n" +
                                  "Quas qui saepe perspiciatis aut asperiores dolor dolore.\n" +
                                  "Aliquam temporibus repudiandae magnam non cum aut quia eius vel.\n" +
                                  "Aut aliquid officia ad.",
                    CreatedAt = Convert.ToDateTime("2020-06-30T16:18:15.6037796+00:00"),
                    FinishedAt = Convert.ToDateTime("2021-04-17T04:55:46.3325709+00:00"),
                    State = TaskState.Created,
                    ProjectId = 2,
                    PerformerId = 13
                },
                new TaskModel()
                {
                    Id = 16,
                    Name = "Inventore nulla sit et eius tempore dignissimos ut velit.",
                    Description = "Consequatur est sed id nemo fugit illo.\n" +
                                  "Minus numquam enim veritatis in sed molestias et.\n" +
                                  "Qui molestiae rerum voluPraesentium autem consequatur magnam et doloribus exercitationem.\n" +
                                  "Aut animi fuga cupiditate debitis atque nisi consequatur consequatur.\n" +
                                  "Cupiditate necessitatibus quo eos sequi earum et quis accusamus.ptatem omnis et.\n" +
                                  "Impedit aliquid ducimus et officia mollitia.\n" +
                                  "Libero voluptatem et libero.\n" +
                                  "Sint et architecto quae tenetur est.",
                    CreatedAt = Convert.ToDateTime("2020-07-01T00:15:53.0919868+00:00"),
                    FinishedAt = Convert.ToDateTime("2020-09-30T06:30:46.6071674+00:00"),
                    State = TaskState.Started,
                    ProjectId = 2,
                    PerformerId = 15
                },
                new TaskModel()
                {
                    Id = 17,
                    Name = "Eaque labore officia placeat est error blanditiis quos.",
                    Description = "Tenetur libero maiores fugit eos voluptatem id maxime dolores ducimus.\n" +
                                  "Maiores omnis quia.\n" +
                                  "Nobis quas optio iste qui autem odit tempora qui ratione.\n" +
                                  "Asperiores ut doloremque odio eius.\n" +
                                  "Qui porro sed autem ut sed.",
                    CreatedAt = Convert.ToDateTime("2020-07-01T03:42:41.9350754+00:00"),
                    FinishedAt = Convert.ToDateTime("2021-06-29T04:11:51.2040436+00:00"),
                    State = TaskState.Canceled,
                    ProjectId = 5,
                    PerformerId = 17
                },
                new TaskModel()
                {
                    Id = 18,
                    Name = "Praesentium aut rerum velit ad non.",
                    Description = "Occaecati nulla dignissimos deserunt.\n" +
                                  "Consequatur doloremque quaerat porro inventore incidunt cumque nulla inventore sed.\n" +
                                  "Porro delectus reiciendis occaecati nisi temporibus ea.\n" +
                                  "Fugiat eligendi quo fugit et nihil.",
                    CreatedAt = Convert.ToDateTime("2020-06-30T15:22:49.9011512+00:00"),
                    FinishedAt = Convert.ToDateTime("2021-04-03T08:23:15.6621481+00:00"),
                    State = TaskState.Started,
                    ProjectId = 8,
                    PerformerId = 17
                },
                new TaskModel()
                {
                    Id = 19,
                    Name = "In odit omnis dolores quo similique placeat ex.",
                    Description = "Perspiciatis et laborum et cum recusandae rerum repellat.\n" +
                                  "Repudiandae pariatur minus corrupti doloribus omnis ad aut repudiandae.\n" +
                                  "Nostrum qui assumenda qui eveniet.\n" +
                                  "Non in nisi in quasi excepturi commodi et nam.\n" +
                                  "Ut vitae ea odio aut ut rem nobis inventore aspernatur.\n" +
                                  "Repellendus itaque et nostrum recusandae explicabo.",
                    CreatedAt = Convert.ToDateTime("2020-07-01T14:23:52.023681+00:00"),
                    FinishedAt = Convert.ToDateTime("2021-06-23T21:11:12.5081451+00:00"),
                    State = TaskState.Finished,
                    ProjectId = 7,
                    PerformerId = 14
                },
                new TaskModel()
                {
                    Id = 20,
                    Name = "Rerum a eum minus repellendus.",
                    Description = "Vitae temporibus adipisci similique voluptatum vel facere fugit vitae.\n" +
                                  "Et qui qui quidem est odit eos quam.\n" +
                                  "Nisi natus dolores fugiat consequatur quo est dolorum.\n" +
                                  "Accusamus voluptatibus expedita est expedita sapiente vero dolorem aspernatur commodi.\n" +
                                  "Dolore dolorem asperiores eligendi totam repudiandae ipsum.\n" +
                                  "Porro illum quos error voluptatibus maxime totam.",
                    CreatedAt = Convert.ToDateTime("2020-07-01T04:58:39.3816512+00:00"),
                    FinishedAt = Convert.ToDateTime("2020-12-26T02:20:28.7607853+00:00"),
                    State = TaskState.Canceled,
                    ProjectId = 9,
                    PerformerId = 7
                }
            };
        }
    }
}
