using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPILinq.DAL.Models;
using WebAPILinq.UnitOfWork.Repositories;

namespace WebAPILinq.UnitOfWork
{
    public class UnitOfWork: IUnitOfWork
    {
        private TaskRepository _taskRepository;
        private ProjectRepository _projectRepository;
        private UserRepository _userRepository;
        private TeamRepository _teamRepository;
        
        public IBaseRepository<TaskModel> TaskRepository => _taskRepository ??= new TaskRepository();
        public IBaseRepository<ProjectModel> ProjectRepository => _projectRepository ??= new ProjectRepository();
        public IBaseRepository<UserModel> UserRepository => _userRepository ??= new UserRepository();
        public IBaseRepository<TeamModel> TeamRepository => _teamRepository ??= new TeamRepository();
    }
}