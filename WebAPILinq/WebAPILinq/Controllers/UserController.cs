using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using WebAPILinq.BLL.Services;
using WebAPILinq.DAL.Models;

namespace WebAPILinq.Controllers
{
    
    [ApiController]
    [Route("api/")]
    public class UserController : Controller
    {
        private IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet]
        [Route("users")]
        public IActionResult GetAll()
        {
            var result = _userService.GetAllUsers();
            
            if (result.Any())
            {
                return Ok(result);
            }
            else
            {
                return NotFound();
            }
        }
        [HttpGet]
        [Route("users/withSortedTasks")]
        public IActionResult GetUsersWithSortedTasks()
        {
            var result = _userService.GetUsersWithSortedTasks();
            
            if (result.Any())
            {
                return Ok(result);
            }
            else
            {
                return NotFound();
            }
        }
        
        [HttpGet]
        [Route("users/detailedInfo/{userId}")]
        public IActionResult GetUsersDetailedInfo(int userId)
        {
            
            var result = _userService.GetDetailedInformByUserId(userId);
            
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                return NotFound("This user doesn't exist or this user doesn't have any projects");
            }
        }

        [HttpPost]
        [Route("users")]
        public IActionResult AddUser([FromBody] UserModel user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Body request is bad");
            }
            
            string strMistakes;
            
            try
            {
                _userService.AddUser(user);
                return Created("", user);
            }
            catch (InvalidCastException e)
            {
                strMistakes = e.Message;
            }
            catch (NullReferenceException e)
            {
                strMistakes = e.Message;
            }
            catch (Exception e)
            {
                strMistakes = e.Message;
            }

            return BadRequest(strMistakes);
        }
        
        [HttpDelete]
        [Route("users/{userId}")]
        public IActionResult Delete(int userId)
        {
            string strMistakes;
            try
            {
                _userService.DeleteUser(userId);
                return Ok("User was deleted");
            }
            catch (InvalidOperationException ex)
            {
                strMistakes = ex.Message;
            }
            catch (Exception ex)
            {
                strMistakes = ex.Message;
            }

            return BadRequest(strMistakes);
        }

        [HttpPut]
        [Route("users")]
        public IActionResult Update(UserModel userModel)
        {
            string strMistakes;
            
            if (!ModelState.IsValid)
            {
                return BadRequest("Body request is bad");
            }
            
            try
            {
                _userService.UpdateUser(userModel);
                return Ok("User was updated");
            }
            catch (InvalidCastException e)
            {
                strMistakes = e.Message;
            }
            catch (NullReferenceException e)
            {
                strMistakes = e.Message;
            }
            catch (Exception e)
            {
                strMistakes = e.Message;
            }

            return BadRequest(strMistakes);
        }
        
    }
}