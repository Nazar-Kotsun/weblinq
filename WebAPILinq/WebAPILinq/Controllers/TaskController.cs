using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebAPILinq.BLL.Services;
using WebAPILinq.DAL.Models;

namespace WebAPILinq.Controllers
{
    [ApiController]
    [Route("api/")]
    public class TaskController : Controller
    {

        private readonly ITaskService _taskService;

        public TaskController(ITaskService taskService)
        {
            _taskService = taskService;
        }
        
        [HttpGet]
        [Route("tasks")]
        public IActionResult  GetAll()
        {
            var result = _taskService.GetAllTasks();

            if (result.Any())
            {
                return Ok(result);
            }
            else
            {
                return NotFound();
            }
        }

        [HttpGet]
        [Route("tasks/byUserID/{userId}")]
        public IActionResult GetTasksByUserId(int userId)
        {
            var result = _taskService.GetTasksByUserId(userId);
            
            if (result.Any())
            {
                return Ok(result);
            }
            else
            {
                return NotFound();
            }
        }
        
        [HttpGet]
        [Route("tasks/finishedInCurrentYear/{userId}")]
        public IActionResult GetTasksFinishedInCurrentYear(int userId)
        {
            var result = _taskService.GetTasksFinishedInCurrentYear(userId);
            
            if (result.Any())
            {
                return Ok(result);
            }
            else
            {
                return NotFound();
            }
        }
        
        [HttpPost]
        [Route("tasks")]
        public IActionResult AddTask([FromBody] TaskModel taskModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Body request is bad");
            }
            
            string strMistakes;
            
            try
            {
                _taskService.AddTask(taskModel);
                return Created("", "Task was created!");
            }
            catch (InvalidCastException e)
            {
                strMistakes = e.Message;
            }
            catch (NullReferenceException e)
            {
                strMistakes = e.Message;
            }
            catch (Exception e)
            {
                strMistakes = e.Message;
            }

            return BadRequest(strMistakes);
        }
        
        [HttpDelete]
        [Route("tasks/{taskId}")]
        public IActionResult DeleteTask(int taskId)
        {
            string strMistakes;
            try
            {
                _taskService.DeleteTask(taskId);
                return Ok("Task was deleted");
            }
            catch (InvalidOperationException ex)
            {
                strMistakes = ex.Message;
            }
            catch (NullReferenceException ex)
            {
                strMistakes = ex.Message;
            }
            catch (Exception ex)
            {
                strMistakes = ex.Message;
            }

            return BadRequest(strMistakes);
        }
        
        [HttpPut]
        [Route("tasks")]
        public IActionResult UpdateProject(TaskModel taskModel)
        {
            string strMistakes;
            
            if (!ModelState.IsValid)
            {
                return BadRequest("Body request is bad");
            }
            
            try
            {
                _taskService.UpdateTask(taskModel);
                return Ok("Task was updated");
            }
            catch (InvalidCastException e)
            {
                strMistakes = e.Message;
            }
            catch (NullReferenceException e)
            {
                strMistakes = e.Message;
            }
            catch (Exception e)
            {
                strMistakes = e.Message;
            }

            return BadRequest(strMistakes);
        }
        
    }
}