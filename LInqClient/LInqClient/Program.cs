﻿using System;
using System.Threading.Tasks;
using LInqClient.HttpLinqClient;
using LInqClient.Menu;
using LInqClient.Models;

namespace LInqClient
{
    class Program
    {
        static async Task Main(string[] args)
        {
            
            MainMenu menu = new MainMenu();
            await menu.StartMenu();

        }
    }
}