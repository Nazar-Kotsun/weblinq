using System;
using System.Collections.Generic;
using WebAPILinq.DAL.Models;

namespace WebAPILinq.Helpers
{
    public static class UserModelFactory
    {
        public static IEnumerable<UserModel> CreateUsers()
        {
            return new List<UserModel>
            {
                new UserModel
                {
                    Id = 1,
                    FirstName = "Jordane",
                    LastName = "Walker",
                    Email = "Jordane.Walker@gmail.com",
                    Birthday = Convert.ToDateTime("2005-10-28T21:26:53.0193606+00:00"),
                    RegisteredAt = Convert.ToDateTime("2020-06-24T09:11:10.5691173+00:00"),
                    TeamId = 5
                },
                new UserModel
                {
                    Id = 2,
                    FirstName = "Kolby",
                    LastName = "Jones",
                    Email = "Kolby_Jones@yahoo.com",
                    Birthday = Convert.ToDateTime("2012-01-23T10:37:51.9412291+00:00"),
                    RegisteredAt = Convert.ToDateTime("020-06-27T20:20:55.7940233+00:00"),
                    TeamId = 1
                },
                new UserModel
                {
                    Id = 3,
                    FirstName = "Mabelle",
                    LastName = "Miller",
                    Email = "Mabelle.Miller12@hotmail.com",
                    Birthday = Convert.ToDateTime("2008-09-29T15:28:43.7369629+00:00"),
                    RegisteredAt = Convert.ToDateTime("2020-06-22T12:35:42.5834106+00:00"),
                    TeamId = 3
                },
                new UserModel
                {
                    Id = 4,
                    FirstName = "Verla",
                    LastName = "Bechtelar",
                    Email = "Verla.Bechtelar62@gmail.com",
                    Birthday = Convert.ToDateTime("2008-07-14T20:47:23.7087566+00:00"),
                    RegisteredAt = Convert.ToDateTime("2020-07-01T05:22:15.869556+00:00"),
                    TeamId = 5
                },
                new UserModel
                {
                    Id = 5,
                    FirstName = "Retha",
                    LastName = "Will",
                    Email = "Retha67@yahoo.com",
                    Birthday = Convert.ToDateTime("2007-09-20T10:05:41.1527935+00:00"),
                    RegisteredAt = Convert.ToDateTime("2020-06-23T18:09:18.5038054+00:00"),
                    TeamId = 3
                },
                new UserModel
                {
                    Id = 6,
                    FirstName = "Roberto",
                    LastName = "Ruecker",
                    Email = "Roberto.Ruecker35@yahoo.com",
                    Birthday = Convert.ToDateTime("2007-02-26T23:18:31.1405084+00:00"),
                    RegisteredAt = Convert.ToDateTime("2020-06-01T23:17:26.4309832+00:00"),
                    TeamId = 1
                },
                new UserModel
                {
                    Id = 7,
                    FirstName = "Tracy",
                    LastName = "Schinner",
                    Email = "Tracy.Schinner65@gmail.com",
                    Birthday = Convert.ToDateTime("2003-07-08T12:00:24.3757499+00:00"),
                    RegisteredAt = Convert.ToDateTime("2020-06-23T14:37:38.0498331+00:00"),
                    TeamId = 3
                },
                new UserModel
                {
                    Id = 8,
                    FirstName = "Norma",
                    LastName = "Gislason",
                    Email = "Norma_Gislason@yahoo.com",
                    Birthday = Convert.ToDateTime("2003-04-10T20:50:03.5493682+00:00"),
                    RegisteredAt = Convert.ToDateTime("2020-05-28T17:02:02.295508+00:00"),
                    TeamId = 5
                },
                new UserModel
                {
                    Id = 9,
                    FirstName = "Luisa",
                    LastName = "Lind",
                    Email = "Luisa92@yahoo.com",
                    Birthday = Convert.ToDateTime("2019-06-10T02:27:47.4844129+00:00"),
                    RegisteredAt = Convert.ToDateTime("2020-06-22T19:42:43.764246+00:00"),
                    TeamId = 4
                },
                new UserModel
                {
                    Id = 10,
                    FirstName = "Edward",
                    LastName = "Kuvalis",
                    Email = "Edward_Kuvalis@yahoo.com",
                    Birthday = Convert.ToDateTime("2008-01-01T09:16:36.4548263+00:00"),
                    RegisteredAt = Convert.ToDateTime("2020-06-02T23:16:31.267727+00:00"),
                    TeamId = null
                },
                new UserModel
                {
                    Id = 11,
                    FirstName = "Jayde",
                    LastName = "Pouros",
                    Email = "Jayde22@hotmail.com",
                    Birthday = Convert.ToDateTime("2012-09-18T12:52:12.4424499+00:00"),
                    RegisteredAt = Convert.ToDateTime("2020-06-29T13:40:13.993952+00:00"),
                    TeamId =  null
                },
                new UserModel
                {
                    Id = 12,
                    FirstName = "Sherman",
                    LastName = "Kuhlman",
                    Email = "Sherman_Kuhlman44@hotmail.com",
                    Birthday = Convert.ToDateTime("2003-01-30T17:45:04.7477993+00:00"),
                    RegisteredAt = Convert.ToDateTime("2020-06-11T22:12:48.7118931+00:00"),
                    TeamId = 5
                },
                new UserModel
                {
                    Id = 13,
                    FirstName = "Issac",
                    LastName = "Rath",
                    Email = "Issac.Rath@gmail.com",
                    Birthday = Convert.ToDateTime("2020-04-01T05:47:34.5794826+00:00"),
                    RegisteredAt = Convert.ToDateTime("2020-06-18T07:48:28.9542609+00:00"),
                    TeamId = 4
                },
                new UserModel
                {
                    Id = 14,
                    FirstName = "Kirsten",
                    LastName = "Braun",
                    Email = "Kirsten_Braun49@gmail.com",
                    Birthday = Convert.ToDateTime("2019-03-31T07:06:27.880072+00:00"),
                    RegisteredAt = Convert.ToDateTime("2020-06-03T14:20:09.6735222+00:00"),
                    TeamId = 4
                },
                new UserModel
                {
                    Id = 15,
                    FirstName = "Tamara",
                    LastName = "Brakus",
                    Email = "Tamara58@gmail.com",
                    Birthday = Convert.ToDateTime("2016-09-05T15:10:36.5090538+00:00"),
                    RegisteredAt = Convert.ToDateTime("2020-06-08T13:04:31.6144169+00:00"),
                    TeamId = 4
                },
                new UserModel
                {
                    Id = 16,
                    FirstName = "Hilton",
                    LastName = "Kemmer",
                    Email = "Hilton.Kemmer84@gmail.com",
                    Birthday = Convert.ToDateTime("2010-11-19T21:50:45.8587891+00:00"),
                    RegisteredAt = Convert.ToDateTime("2020-07-01T07:00:16.6860452+00:00"),
                    TeamId = 2
                },
                new UserModel
                {
                    Id = 17,
                    FirstName = "Delmer",
                    LastName = "Wunsch",
                    Email = "Delmer9@gmail.com",
                    Birthday = Convert.ToDateTime("2008-04-10T13:16:49.9926847+00:00"),
                    RegisteredAt = Convert.ToDateTime("2020-05-27T21:57:23.5397637+00:00"),
                    TeamId = 2
                },
                new UserModel
                {
                    Id = 18,
                    FirstName = "Lupe",
                    LastName = "Harber",
                    Email = "Lupe91@gmail.com",
                    Birthday = Convert.ToDateTime("2002-07-26T20:27:55.9827619+00:00"),
                    RegisteredAt = Convert.ToDateTime("2020-05-22T17:21:56.0998576+00:00"),
                    TeamId = 5
                },
                new UserModel
                {
                    Id = 19,
                    FirstName = "Cedrick",
                    LastName = "Lemke",
                    Email = "edrick33@hotmail.com",
                    Birthday = Convert.ToDateTime("2004-11-26T06:18:16.7968598+00:00"),
                    RegisteredAt = Convert.ToDateTime("2020-05-22T04:53:58.617495+00:00"),
                    TeamId = 1
                },
                new UserModel
                {
                    Id = 20,
                    FirstName = "Zane",
                    LastName = "Will",
                    Email = "Zane.Will@yahoo.com",
                    Birthday = Convert.ToDateTime("2016-10-17T11:55:25.834367+00:00"),
                    RegisteredAt = Convert.ToDateTime("2020-06-05T11:13:47.4415614+00:00"),
                    TeamId = null
                },
                
            };
        }
    }
}
