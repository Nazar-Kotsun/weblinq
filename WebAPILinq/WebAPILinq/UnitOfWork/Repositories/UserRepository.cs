using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using WebAPILinq.DAL.Models;
using WebAPILinq.Helpers;

namespace WebAPILinq.UnitOfWork.Repositories
{
    public class UserRepository : IBaseRepository<UserModel>
    {
        private List<UserModel> _users;

        public UserRepository()
        {
            _users = UserModelFactory.CreateUsers().ToList();
        }

        public void Create(UserModel entity) => _users.Add(entity);

        public UserModel GetById(int id) => _users.Find((user) => user.Id == id);
        public IEnumerable<UserModel> GetAll() => _users;
        
        public void Update(UserModel entity)
        { 
            Delete(entity.Id);
            Create(entity);
        }

        public void Delete(int id) => _users.Remove
            (_users.Find((user) => user.Id == id)
        );
    }
}