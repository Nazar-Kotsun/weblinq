using System.Collections.Generic;
using WebAPILinq.DAL.Models;

namespace WebAPILinq.BLL.Services
{
    public interface ITeamService
    {
        IEnumerable<TeamModel> GetAllTeams();
        IEnumerable<TeamUsersDTO> GetTeamsWithUsers();
        void AddTeam(TeamModel teamModel);
        void DeleteTeam(int teamId);
        void UpdateTeam(TeamModel teamModel);
    }
}