using System.Collections.Generic;
using System.Threading.Tasks;
using WebAPILinq.DAL.Models;

namespace WebAPILinq.BLL.Services
{
    public interface ITaskService
    {
        IEnumerable<TaskModel> GetAllTasks();
        IEnumerable<TaskModel> GetTasksByUserId(int userId);
        IEnumerable<TaskDTO> GetTasksFinishedInCurrentYear(int userId);
        void AddTask(TaskModel taskModel);
        void DeleteTask(int taskId);
        void UpdateTask(TaskModel taskModel);
    }
}