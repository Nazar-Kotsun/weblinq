using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;

namespace LInqClient.Models.DTO
{
    public class UserTasksDTO
    {
        [Required]
        [JsonPropertyName("userId")]
        public int UserId { get; set; }
        [Required]
        [JsonPropertyName("userFirstName")]
        public string UserFirstName { get; set; }
        [Required]
        [JsonPropertyName("userLastName")]
        public string UserLastName { get; set; }
        [Required]
        [JsonPropertyName("email")]
        public string Email { get; set; }
        [Required]
        [JsonPropertyName("birthday")]
        public DateTime Birthday { get; set; }
        [Required]
        [JsonPropertyName("userRegisteredAt")]
        public DateTime UserRegisteredAt { get; set; }
        [Required]
        [JsonPropertyName("teamId")]
        public int? TeamId { get; set; }
        
        [Required]
        [JsonPropertyName("listTasks")]
        public IEnumerable<TaskModel> ListTasks { get; set; }

        public override string ToString()
        {
            return $"UserId: {UserId}\n" +
                   $"UserFirstName: {UserFirstName}\n" +
                   $"UserLastName: {UserLastName}\n" +
                   $"Email: {Email}\n" +
                   $"Birthday: {Birthday}\n" +
                   $"UserRegisteredAt: {UserRegisteredAt}\n" +
                   $"TeamId: {TeamId}\n" +
                   $"Count of tasks: {ListTasks.ToList().Count}";
        }
    }
}